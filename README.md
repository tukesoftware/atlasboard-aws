This is a package which contains [Atlasboard](http://atlasboard.bitbucket.org/) widgets and jobs for querying and displaying [AWS](http://aws.amazon.com/) data on a dashboard.

Below is an example of some of the widgets available:

![2015-10-06_16-44-52.png](https://bitbucket.org/repo/AzRd6e/images/628623720-2015-10-06_16-44-52.png)

Changelog
============

v3.2.3
------------
* Minor fix to CloudWatch log events widget.

v3.2.2
------------
* Add new widget and job for displaying CloudWatch log events in a news ticker style.

v3.2.0
------------
* Add new widget and job for displaying CloudWatch alarm status.

v3.1.0
------------
* Removed post install script. Depdency script files are now bundled for simplicity.

v3.0.6
------------
* Added the ability to override specific metric values for each target (i.e. namespace)

v3.0.5
------------
* Added the ability to match Route53 records to their alias and derive the set from that instead of the set id

v3.0.4-alpha.1
------------
* Added a widget for displaying CloudWatch data as a number instead of a graph

v3.0.3
------------
* Added the ability for CloudWatch widget graphs to optionally "darken" and dash graph lines

v3.0.2
------------
* Fixed Issue #5 so the legend on the Record Set Weight widget doesn't distort over time

v3.0.1
------------
* Refactored Record Set Weight widget code

v3.0.0
------------
* Chart.js charts (i.e. the charts you see in all widgets) can now be overridden directly from widget config