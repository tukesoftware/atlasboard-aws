'use strict';

const CloudWatch = require('../../lib/aws/service-wrappers/cloudwatch');

module.exports = {
    onRun : function(config, dependencies, job_callback) {
        var cloudWatch = new CloudWatch(config.region, config.roleResourceName);

        const operations = config.targets.map(target =>
            cloudWatch.getCloudWatchLogEvents(target.logGroup, target.filterPattern, config.period)
            .then(events => {
                return {
                    events: events,
                    transform: target.transform
                };
            })
        );

        Promise.all(operations)
        .then(eventSets => {
            eventSets.forEach(eventSet => {
                if (!eventSet.events || (eventSet.events.length === 0))
                    return;

                eventSet.events.forEach(event => {
                    const originalMessage = event.message;

                    if (eventSet.transform.message) {
                        event.message = originalMessage.replace(new RegExp(eventSet.transform.message.pattern), eventSet.transform.message.replacement);
                    }

                    if (eventSet.transform.detail) {
                        event.detail = originalMessage.replace(new RegExp(eventSet.transform.detail.pattern), eventSet.transform.detail.replacement);
                    }
                });
            });

            job_callback(null, {
                VisualConfig : config.visualConfig,
                Events: [].concat.apply([], eventSets.map(s => s.events)),
                Period: config.period + ((config.interval || 60000) / 1000)
            });
        })
        .catch(error => {
            dependencies.logger.log(`Error getting CloudWatch log events for group: ${config.logGroup}`);
            job_callback(error);
        });
    }
};