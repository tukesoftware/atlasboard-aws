var AWS = require('aws-sdk');
var Enumerable = require('linq');
var When = require('when');
var AutoScaling = require('../../lib/aws/service-wrappers/autoscaling');

module.exports = {
    onRun : function(config, dependencies, job_callback) {        
        var now = new Date();
        var startTime = new Date(now.getTime());
        var endTime = new Date(now.getTime());
        startTime.setHours(startTime.getHours() - 1);
        
        dependencies.logger.log('Getting metrics for: %s', config.visualConfig.title);

        var finalised = false;        
        var autoScaling = new AutoScaling(config.region, config.roleResourceName);    
        autoScaling.getAutoScaleGroup(config.autoScaleGroupName, startTime, endTime, config.metric, config.alarms)
        .then(        
            function(autoScaleGroup) {                        
                if (finalised)
                    return;

                finalised = true;
                job_callback(null, { VisualConfig : config.visualConfig, AutoScaleGroup : autoScaleGroup });            
            }
        )
        .timeout(60000)
        .catch(
            function(error) {         
                dependencies.logger.error('Error getting metrics for: %s', config.visualConfig.title);
                finalised = true;
                job_callback(error);
            }
        );
    }
}