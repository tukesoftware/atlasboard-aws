'use strict';

const CloudWatch = require('../../lib/aws/service-wrappers/cloudwatch');

module.exports = {
    onRun : function(config, dependencies, job_callback) {
        var cloudWatch = new CloudWatch(config.region, config.roleResourceName);
        cloudWatch.getCloudwatchAlarmStatus(config.alarmName)
        .then(status => {
            job_callback(null, { VisualConfig : config.visualConfig, AlarmOkay: (status === 'OK') });
        })
        .catch(error => {
            dependencies.logger.log(`Error getting CloudWatch alarm: ${config.visualConfig.title}`);
            job_callback(error);
        });
    }
};