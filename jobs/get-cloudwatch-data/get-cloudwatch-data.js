var Enumerable = require('linq');
var When = require('when');
var CloudWatch = require('../../lib/aws/service-wrappers/cloudwatch');
var Assign = require('object-assign');

self = module.exports = {
    onRun : function(config, dependencies, job_callback) {      
        var now = new Date();    
        var startTime = new Date(now.getTime());
        var endTime = new Date(now.getTime());
        startTime.setHours(startTime.getHours() - config.metric.hours);

        dependencies.logger.log('Getting metric: %s', config.visualConfig.title);      
        var operations = Enumerable.from(config.targets)
                                   .select(function(target) {
                                      var cloudWatch = new CloudWatch(config.region, target.roleResourceName);                                      
                                      var metric = target.metric ? Assign(target.metric, config.metric) : config.metric;                                      
                                      return cloudWatch.getCloudwatchMetric(metric, target, startTime, endTime);
                                    })
                                   .toArray();

        var finalised = false;
        When.all(operations).then(
            function(metricDatasets) {
                if (finalised)
                    return;            

                finalised = true;
                self.alignDatasets(metricDatasets, startTime, endTime, config.metric.period ? config.metric.period : defaultPeriod, config.metric.statistic);
                job_callback(null, { VisualConfig : config.visualConfig, Metric : config.metric, MetricDatasets : metricDatasets });
            }
        )
        .timeout(60000)
        .catch(
            function(error) {
                dependencies.logger.log('Error getting metric: %s', config.visualConfig.title);            
                finalised = true;
                job_callback(error);
            }
        );
    },

    alignDatasets : function(datasets, startTime, endTime, period, statistic) {
        var dataTicks = Enumerable.from(datasets)
                                       .selectMany('$.Data')
                                       .distinct('$.Ticks');

        if (dataTicks.count() == 0) {
            return;
        }

        var firstTicks = dataTicks.min('$.Ticks');            
        var startPadding = Enumerable.rangeTo(startTime.getTime() < firstTicks ? startTime.getTime() : firstTicks, firstTicks, period * 1000)
                                   .select('{ Ticks : $, ' + statistic + ' : null }')
                                   .toArray();        

        var endPadding = Enumerable.rangeTo(firstTicks, endTime.getTime(), period * 1000)
                                   .select('{ Ticks : $, ' + statistic + ' : null }')
                                   .toArray();                         
                                
        Enumerable.from(datasets)
                  .forEach(function(dataset) {
                        dataset.Data = Enumerable.from(dataset.Data)
                                                   .union(startPadding, '$.Ticks')
                                                   .union(endPadding, '$.Ticks')
                                                   .orderBy('$.Ticks')
                                                   .toArray();
                  });
    }
}