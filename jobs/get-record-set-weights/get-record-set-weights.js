var AWS = require('aws-sdk');
var Enumerable = require('linq');
var When = require('when');
var Route53 = require('../../lib/aws/service-wrappers/route53.js');

module.exports = function(config, dependencies, job_callback) {     
    dependencies.logger.log('Getting recordset weights for record: %s', config.recordSet);

    var finalised = false;
    var route53 = new Route53(config.region, config.roleResourceName);    
    route53.getRecordSetWeights(config.hostedZoneId, config.recordSet, config.aliasPattern)
    .then(function(weights) {
        if (finalised)
            return;

        finalised = true;
        job_callback(null, { VisualConfig : config.visualConfig, Weights : weights });
    })
    .timeout(60000)
    .catch(
        function(error) {
            dependencies.logger.error('Error getting recordset weights for record: %s', config.recordSet);
            finalised = true;
            job_callback(error);
        }
    );
};