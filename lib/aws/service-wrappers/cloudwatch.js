'use strict';

var AWS = require('aws-sdk');
var Enumerable = require('linq');
var When = require('when');
var Colour = require('color');
var ServiceWrapper = require('../service-wrapper');
var Moment = require('moment');

var CloudWatch = function() {
    ServiceWrapper.apply(this, arguments);
};

CloudWatch.prototype = Object.create(ServiceWrapper.prototype);
CloudWatch.prototype.constructor = CloudWatch;

CloudWatch.prototype.getCloudwatchMetric = function(metric, target, startTime, endTime) {
    var credentials = this.credentials;
    return When.promise(function(resolve, reject) {
        var params = {
            Namespace : metric.namespace,
            MetricName : metric.metricName,
            Period : metric.period ? metric.period : 300,
            StartTime : startTime,
            EndTime : endTime,
            Statistics : [ metric.statistic ]
        };

        if (target.unit) {
            params.Unit = target.unit;
        }

        if (target.dimensions) {
            params.Dimensions = Enumerable.from(target.dimensions)
                                          .select("{ 'Name' : $.name, 'Value' : $.value }")
                                          .toArray();
        }
        
        var cloudWatch = new AWS.CloudWatch({ credentials : credentials });
        cloudWatch.getMetricStatistics(params, function(error, data) {
            if (error) {
                reject(error);
                return;
            }

            resolve(data);
        });
    })
    .then(function(data) {
        return {
            Label : target.label,
            Colour : Colour(target.colour).darken(target.darkenBy ? target.darkenBy : 0).rgbString(),
            Dashed : target.dashed ? target.dashed : false,
            ShadowColour : Colour(target.colour).lighten(0.10).rgbString(),
            Lumosity : Colour(target.colour).luminosity(),
            Data : Enumerable.from(data.Datapoints)
                             .doAction(function(datapoint) {
                                 datapoint.Ticks = new Date(datapoint.Timestamp).getTime();
                             })
                             .orderBy('$.Ticks')
                             .toArray()
        };
    });
};

CloudWatch.prototype.getCloudwatchAlarmStatus = function(alarmName) {
    const credentials = this.credentials;
    return new Promise(resolve => {
        const cloudWatch = new AWS.CloudWatch({ credentials : credentials });
        resolve(cloudWatch.describeAlarms({ AlarmNames: [ alarmName ] }).promise()
        .then(data => {
            if (!data.MetricAlarms || (data.MetricAlarms.length === 0))
                throw `Unable to find CloudWatch alarm with name: ${alarmName}`;

            if (data.MetricAlarms.length > 1)
                throw `More than one CloudWatch alarm found with name: ${alarmName}`;

            const alarm = data.MetricAlarms[0];
            return alarm.StateValue;
        }));
    });
};

CloudWatch.prototype.getCloudWatchLogEvents = function(logGroup, filterPattern, period) {
    const credentials = this.credentials;
    return new Promise(resolve => {
        const params = {
            logGroupName: logGroup,
            startTime: Moment.utc().subtract(period, 'seconds').valueOf(),
            filterPattern: filterPattern
        };
        const service = new AWS.CloudWatchLogs({ credentials : credentials });
        return resolve(this.filterCloudWatchLogEvents(service, params));
    });
};

CloudWatch.prototype.filterCloudWatchLogEvents = function(service, params, events = []) {
    return service.filterLogEvents(params).promise()
    .then(data => {
        params.nextToken = data.nextToken;
        events = events.concat(data.events);
        return data.nextToken ? this.filterCloudWatchLogEvents(service, params, events) : events;
    });
};

module.exports = CloudWatch;