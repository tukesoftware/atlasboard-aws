var AWS = require('aws-sdk');
var Enumerable = require('linq');
var When = require('when');
var ServiceWrapper = require('../service-wrapper');

var AutoScaling = function() {
    ServiceWrapper.apply(this, arguments);
}

AutoScaling.prototype = Object.create(ServiceWrapper.prototype);
AutoScaling.prototype.constructor = AutoScaling;

AutoScaling.prototype.getAutoScaleGroup = function(autoScaleGroupName, startTime, endTime, metric, alarms) {
    var self = this;
    return When.promise(function(resolve, reject) {
        var params = {
            AutoScalingGroupNames : [ autoScaleGroupName ]
        };

        var autoScaling = new AWS.AutoScaling({ credentials : self.credentials });
        autoScaling.describeAutoScalingGroups(params, function(error, data) {
            if (error) {
                reject(error);
                return;
            }

            resolve(data);
        });
    })
    .then(function(data) {        
        return When.promise(function(resolve, reject) {            
            var autoScaleGroup = Enumerable.from(data.AutoScalingGroups)
                                           .singleOrDefault(null);

            if (!autoScaleGroup) {
                reject(new Error('No auto scale groups found with the name: %s', autoScaleGroupName));
                return;
            }            

            resolve(autoScaleGroup);
        });
    })
    .then(
        function(autoScaleGroup) {    
            return self.getMetricsForInstances(autoScaleGroup.Instances, startTime, endTime, metric)
                       .then(function(instances) {               
                           return {
                                Name : autoScaleGroup.AutoScalingGroupName,
                                Instances : instances,
                                MinSize : autoScaleGroup.MinSize,
                                MaxSize : autoScaleGroup.MaxSize,
                                CurrentSize : Enumerable.from(instances)                
                                                        .where('$.Status == "running"')
                                                        .count()
                            };    
                       });                
        }
    );
}

AutoScaling.prototype.getMetricsForInstances = function(instances, startTime, endTime, metric) {
    var self = this;   
    return When.promise(function(resolve, reject) {
        if (instances.length == 0) {
            resolve(instances);
        }

        var instanceIds = Enumerable.from(instances)
                                    .select('$.InstanceId')
                                    .toArray();

        var params = {
            InstanceIds : instanceIds,
            IncludeAllInstances : true
        };


        var ec2 = new AWS.EC2({ credentials : self.credentials, region : self.region });
        ec2.describeInstanceStatus(params, function(error, data) {
            if (error) {                
                reject(error);
                return;
            }

            instances = Enumerable.from(instances)
                                  .join(data.InstanceStatuses, '$.InstanceId', '$.InstanceId', function(inner, outer) { inner.Status = outer.InstanceState.Name; return inner; })
                                  .toArray();

            resolve(instances);
        });
    })
    .then(function(instances) {
        var operations =  Enumerable.from(instances)
                                    .select(function(instance) { return self.getInstanceMetric(instance, startTime, endTime, metric); })
                                    .toArray();

        return When.all(operations);
    });    
}   

AutoScaling.prototype.getInstanceMetric = function(instance, startTime, endTime, metric) {        
    var self = this;
    return When.promise(function(resolve, reject) {       
        if (instance.Status != 'running') {            
            resolve({ Datapoints : null });
            return;
        }

        var params = {
            Namespace : "AWS/EC2",
            MetricName : metric.metricName,
            Unit : metric.unit,
            Period : metric.period ? metric.period : 60,
            StartTime : startTime,
            EndTime : endTime,
            Statistics : [ metric.statistic ],
            Dimensions : [ { 
                Name : 'InstanceId',
                Value : instance.InstanceId
            } ]
        };    

        var cloudWatch = new AWS.CloudWatch({ credentials : self.credentials, region : self.region });
        cloudWatch.getMetricStatistics(params, function(error, data) {
            if (error) {
                reject(error);
                return;
            }            

            resolve(data);
        });
    })    
    .then(function(metricData) {      
        var lastDatapoint = Enumerable.from(metricData.Datapoints)                                          
                                      .orderBy('$.Timestamp')
                                      .lastOrDefault();        

        instance.MetricValue = lastDatapoint ? lastDatapoint[metric.statistic] : null;            
        return instance;          
    });
}

module.exports = AutoScaling;