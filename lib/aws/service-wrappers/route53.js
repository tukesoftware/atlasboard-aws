var AWS = require('aws-sdk');
var Enumerable = require('linq');
var When = require('when');
var ServiceWrapper = require('../service-wrapper');

var Route53 = function() {
    ServiceWrapper.apply(this, arguments);    
}

Route53.prototype = Object.create(ServiceWrapper.prototype);
Route53.prototype.constructor = Route53;

Route53.prototype.getRecordSetWeights = function(hostedZoneId, recordSetName, aliasPattern) {
    var credentials = this.credentials;
    return When.promise(function(resolve, reject) {
        var params = {
            HostedZoneId : hostedZoneId,
            StartRecordName : recordSetName,
            StartRecordType : 'A',
            MaxItems : '10'
        };
        
        var route53 = new AWS.Route53({ credentials : credentials });
        route53.listResourceRecordSets(params, function(error, data) {
            if (error) {
                reject(error);
                return;
            }        

            resolve(data);
        });
    })
    .then(function(data) {    
        var aliasPatternRegex = new RegExp(aliasPattern);                 
        return Enumerable.from(data.ResourceRecordSets)
                         .where(function(recordSet) { return recordSet.Name.indexOf(recordSetName) == 0 })
                         .select(function(recordSet) {
                             var set = recordSet.SetIdentifier;                             
                             if (aliasPattern) {                             
                                var aliasPatternMatches = recordSet.AliasTarget.DNSName.match(aliasPatternRegex);
                                if (!aliasPatternMatches) {
                                    throw 'Unable to match DNS alias to pattern: ' + aliasPattern;
                                }      

                                set = aliasPatternMatches[0];
                             }   

                             return { Set : set, Weight : recordSet.Weight }; 
                         })
                         .toArray();        
    });
}

module.exports = Route53;