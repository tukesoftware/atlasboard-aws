var AWS = require('aws-sdk');

const CredentialsCache = {}

module.exports = ServiceWrapper;

function ServiceWrapper(region, roleResourceName) {
    this.region = AWS.config.region = region ? region : 'eu-west-1';

    if (!AWS.config.credentials) {
        AWS.config.credentials = new AWS.EC2MetadataCredentials();
    }

    if (roleResourceName) {
        if (!(roleResourceName in CredentialsCache)) {
            CredentialsCache[roleResourceName] = new AWS.TemporaryCredentials({ RoleArn: roleResourceName });
        }

        this.credentials = CredentialsCache[roleResourceName];
    } else {
        this.credentials = AWS.config.credentials;
    }
}