widget = {
    onData : function(widget, data) {
        if (!widget.isInitialised) {
            this.setWidgetTitle(widget, data.VisualConfig.title);
            widget.isInitialised = true;
        }

        var alarmStatusElement = $('canvas.alarm-status', widget).get(0);
        alarmStatusElement.width = 50;
        alarmStatusElement.height = 50;

        var alarmStatusContext = alarmStatusElement.getContext('2d');
        alarmStatusContext.beginPath();
        alarmStatusContext.arc(20, 20, 10, 0, Math.PI * 2, true);
        alarmStatusContext.closePath();
        alarmStatusContext.fillStyle = data.AlarmOkay ? '#859900' : '#dc322f';
        alarmStatusContext.fill();
    },

    setWidgetTitle : function(widget, title) {
        if (!title)
            return;

        $('h2', widget).text(title);
    }
}