widget = {  
    onData : function(widget, data) {
        if (!widget.isInitialised) {
            this.setWidgetTitle(widget, data.VisualConfig.title);
        }

        var dataset = Enumerable.from(data.MetricDatasets).first();        
        var value = Enumerable.from(dataset.Data)
                              .where('$.' + data.Metric.statistic)
                              .select('$.' + data.Metric.statistic)
                              .last();
            
        var valueText = value.toFixed(data.VisualConfig.roundedTo ? data.VisualConfig.roundedTo : 2);
        var valueElement = $('div.value', widget);
        valueElement.text(valueText);
        valueElement.css('color', this.getStatusColour(value, data.VisualConfig.threasholds));

        $('div.unit', widget).text(data.Metric.unit.toLowerCase());
    },

    setWidgetTitle : function(widget, title) {
        if (!title)
            return;

        $('h2', widget).text(title); 
    },

    getStatusColour : function(value, threasholds) {        
        return Enumerable.from(threasholds)
                         .where(function (t) { return (value >= (t.min ? t.min : 0)) && (value <= (t.max ? t.max : Infinity)); })
                         .select('$.colour')
                         .single();        
    }
};