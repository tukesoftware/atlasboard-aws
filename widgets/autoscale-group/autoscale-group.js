widget = {
    onData : function(widget, data) {        
        var graphData = this.buildGraphData(data.AutoScaleGroup, data.VisualConfig)        
        this.buildSummary(widget, data.AutoScaleGroup);

        if (!widget.isInitialised) {
            this.setWidgetTitle(widget, data.VisualConfig.title);
            this.initialiseGraph(widget, graphData, data.VisualConfig);
        } else {
            this.processGraphData(widget, graphData);
        }
    },
    
    setWidgetTitle : function(widget, title) {
        if (!title)
            return;

        $('h2', widget).text(title); 
    },

    buildSummary : function(widget, autoscaleGroup) {
        var definitionList = '<dl>' +
                                 '<dt>Min</dt>' +
                                 '<dd>' + autoscaleGroup.MinSize + '</dd>' +
                                 '<dt>Max</dt>' +
                                 '<dd>' + autoscaleGroup.MaxSize + '</dd>' +
                                 '<dt>Now</dt>' +
                                 '<dd>' + autoscaleGroup.CurrentSize + '</dd>' +
                             '</dl>'

        var summary = $('dl', widget);

        if (summary.length) {
            summary.replaceWith(definitionList);
        } else {
            $('h2', widget).after(definitionList);
        }
    },

    initialiseGraph : function(widget, graphData, config) {
        var canvas = $('canvas', widget).get(0);
        var context = canvas.getContext('2d');

        canvas.width = config.graphWidth;
        canvas.height = config.graphHeight;

        var defaultOptions = {            
            scaleShowVerticalLines : false,       
            barStrokeWidth : 1,         
            showTooltips : false,
            scaleShowGridLines : false,            
            scaleLineColor : '#586e75',
            scaleFontColor : '#839496',            
            animation : false            
        };        

        var options = Chart.helpers.merge(defaultOptions, config.chartOverrides);

        this.extendBarGraph();
        widget.Graph = new Chart(context).BarExtended(graphData, options);
        widget.isInitialised = true;
    },

    buildGraphData : function(autoscaleGroup, config) {
        var padding = Enumerable.range(0, (autoscaleGroup.MaxSize - autoscaleGroup.Instances.length))
                                .select('{ InstanceId : "", MetricValue : null }')
                                .toArray();

        var instances = Enumerable.from(autoscaleGroup.Instances)                                  
                                  .orderBy(function(instance) { return instance.Status == 'running' ? 0 : 1 })
                                  .union(padding);

        return {            
            labels : instances.select('$.InstanceId.substring(0, 5)').toArray(),
            datasets : [
                {
                    fillColor : config.fillColour,
                    strokeColor : config.fillColour,
                    active : instances.select('$.LifecycleState == "InService"').toArray(),
                    data : instances.select('$.MetricValue').toArray()
                }
            ]
        };
    },

    processGraphData : function(widget, updatedGraphData) {
        widget.Graph.initialize(updatedGraphData);
    },

    extendBarGraph : function() {
        Chart.types.Bar.extend({
            name : "BarExtended",

            initialize:  function(data) {
                var helpers = Chart.helpers;
                var options = this.options;

                this.ScaleClass = Chart.Scale.extend({
                    offsetGridLines : true,
                    calculateBarX : function(datasetCount, datasetIndex, barIndex) {
                        var xWidth = this.calculateBaseWidth(),
                            xAbsolute = this.calculateX(barIndex) - (xWidth / 2),
                            barWidth = this.calculateBarWidth(datasetCount);

                        return xAbsolute + (barWidth * datasetIndex) + (datasetIndex * options.barDatasetSpacing) + barWidth / 2;
                    },
                    calculateBaseWidth : function() {
                        return (this.calculateX(1) - this.calculateX(0)) - (2 * options.barValueSpacing);
                    },
                    calculateBarWidth : function(datasetCount) {
                        var baseWidth = this.calculateBaseWidth() - ((datasetCount - 1) * options.barDatasetSpacing);
                        return (baseWidth / datasetCount);
                    }
                });

                this.datasets = [];

                if (this.options.showTooltips) {
                    helpers.bindEvents(this, this.options.tooltipEvents, function(evt) {
                        var activeBars = (evt.type !== 'mouseout') ? this.getBarsAtEvent(evt) : [];

                        this.eachBars(function(bar) {
                            bar.restore(['fillColor', 'strokeColor']);
                        });
                        helpers.each(activeBars, function(activeBar) {
                            activeBar.fillColor = activeBar.highlightFill;
                            activeBar.strokeColor = activeBar.highlightStroke;
                        });
                        this.showTooltip(activeBars);
                    });
                }

                this.BarClass = Chart.Rectangle.extend({
                    strokeWidth : this.options.barStrokeWidth,
                    showStroke : this.options.barShowStroke,
                    ctx : this.chart.ctx
                });

                helpers.each(data.datasets,function(dataset,datasetIndex) {

                    var datasetObject = {
                        label : dataset.label || null,
                        fillColor : dataset.fillColor,
                        strokeColor : dataset.strokeColor,
                        bars : []
                    };

                    this.datasets.push(datasetObject);

                    helpers.each(dataset.data, function(dataPoint, index) {
                        datasetObject.bars.push(new this.BarClass({
                            value : dataPoint,
                            label : data.labels[index],
                            datasetLabel: dataset.label,
                            strokeColor : dataset.strokeColor,
                            fillColor : dataset.fillColor,
                            active : dataset.active[index],
                            highlightFill : dataset.highlightFill || dataset.fillColor,
                            highlightStroke : dataset.highlightStroke || dataset.strokeColor
                        }));
                    }, this);

                }, this);

                this.buildScale(data.labels);

                this.BarClass.prototype.base = this.scale.endPoint;

                this.eachBars(function(bar, index, datasetIndex) {
                    helpers.extend(bar, {
                        width : this.scale.calculateBarWidth(this.datasets.length),
                        x: this.scale.calculateBarX(this.datasets.length, datasetIndex, index),
                        y: this.scale.endPoint
                    });

                    if (!bar.active) {                        
                        bar.fillColor = 'rgba(0,0,0,0)';
                    }

                    bar.save();
                }, this);

                this.render();
            }        
        })
    }
}