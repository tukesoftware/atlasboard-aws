widget = {
    onData : function(widget, data) {
        var graphData = this.buildGraphData(data.MetricDatasets, data.Metric.statistic)
        
        if (!widget.isInitialised) {
            this.setWidgetTitle(widget, data.VisualConfig.title);
            this.initialiseGraph(widget, graphData, data.VisualConfig);       

            if (!data.VisualConfig.hideLegend) {
                this.generateLegend(widget);            
            }
        }
        else {
            this.processGraphData(widget, graphData);
        }
    },

    buildGraphData : function(datasets, statistic) {
        TimeLabel = function(ticks) {
            this.Ticks = ticks;
            this.Label = moment(ticks).format('HH:mm')
        };

        TimeLabel.prototype.toString = function() {        
            return this.Label;
        };

        return {
            labels : Enumerable.from(datasets)
                               .selectMany('$.Data')
                               .distinct('$.Ticks')
                               .orderBy('$.Ticks')        
                               .select(function(datapoint) { return new TimeLabel(datapoint.Ticks); })
                               .toArray(),
            
            datasets : Enumerable.from(datasets)
                                 .select(function (dataset) {
                                     return {
                                         label : dataset.Label,
                                         strokeColor: dataset.Colour,
                                         strokeShadowColor: dataset.ShadowColour,
                                         dashed: dataset.Dashed,                   
                                         lumosity: dataset.Lumosity,
                                         data : Enumerable.from(dataset.Data)
                                                           .select('$.' + statistic)    
                                                           .toArray()
                                     };
                                 })
                                 .orderBy('$.lumosity')
                                 .toArray()
        };        
    },

    setWidgetTitle : function(widget, title) {
        if (!title)
            return;

        $('h2', widget).text(title); 
    },

    initialiseGraph : function(widget, graphData, config) {
        var canvas = $('canvas', widget).get(0);
        var context = canvas.getContext('2d');

        canvas.width = config.graphWidth;
        canvas.height = config.graphHeight;

        var defaultOptions = {
            datasetFill : false,
            pointDot : false,                        
            showTooltips : false,
            animation : false,     
            scaleShowGridLines : false,
            scaleLineColor : '#586e75',
            scaleFontColor : '#839496',
            legendTemplate : "<div class=\"legend\">" +
                                 "<% for (var i = 0; i < datasets.length; i++) { %>" +
                                     "<% if (datasets[i].label) { %><h3><span style=\"background-color: <%= datasets[i].strokeColor %>\" /><%= datasets[i].label %></h3><% } %>" +
                                 "<% } %>" +
                             "</div>"
        };
        
        var options = Chart.helpers.merge(defaultOptions, config.chartOverrides);        

        this.extendLineGraph();
        widget.Graph = new Chart(context).LineExtended(graphData, options);
        widget.isInitialised = true;
    },

    generateLegend : function(widget) {
        var legend = widget.Graph.generateLegend();
        $('h2', widget).after(legend);    
    },

    processGraphData : function(widget, updatedGraphData) {        
        widget.Graph.initialize(updatedGraphData);
    },

    extendLineGraph : function() {
        Chart.types.Line.extend({
            name: "LineExtended",

            initialize: function(data) {
                var helpers = Chart.helpers;
                
                this.PointClass = Chart.Point.extend({
                    strokeWidth: this.options.pointDotStrokeWidth,
                    radius: this.options.pointDotRadius,
                    display: this.options.pointDot,
                    hitDetectionRadius: this.options.pointHitDetectionRadius,
                    ctx: this.chart.ctx,
                    inRange: function(mouseX) {
                        return (Math.pow(mouseX - this.x, 2) < Math.pow(this.radius + this.hitDetectionRadius, 2));
                    }
                });

                this.datasets = [];

                if (this.options.showTooltips) {
                    helpers.bindEvents(this, this.options.tooltipEvents, function(evt) {
                        var activePoints = (evt.type !== 'mouseout') ? this.getPointsAtEvent(evt) : [];
                        this.eachPoints(function(point) {
                            point.restore(['fillColor', 'strokeColor']);
                        });
                        helpers.each(activePoints, function(activePoint) {
                            activePoint.fillColor = activePoint.highlightFill;
                            activePoint.strokeColor = activePoint.highlightStroke;
                        });
                        this.showTooltip(activePoints);
                    });
                }

                helpers.each(data.datasets, function(dataset) {
                    var datasetObject = {
                        label: dataset.label || null,
                        fillColor: dataset.fillColor,
                        strokeColor: dataset.strokeColor,
                        dashed: dataset.dashed,
                        strokeShadowColor: dataset.strokeShadowColor,
                        pointColor: dataset.pointColor,
                        pointStrokeColor: dataset.pointStrokeColor,
                        points: []
                    };

                    this.datasets.push(datasetObject);

                    helpers.each(dataset.data, function(dataPoint, index) {
                        if (helpers.isNumber(dataPoint) || dataPoint === null) {
                            datasetObject.points.push(new this.PointClass({
                                ignore: dataPoint === null,
                                value: dataPoint,
                                label: data.labels[index],
                                datasetLabel: dataset.label,                                
                                strokeColor: dataset.pointStrokeColor,                                
                                fillColor: dataset.pointColor,
                                highlightFill: dataset.pointHighlightFill || dataset.pointColor,
                                highlightStroke: dataset.pointHighlightStroke || dataset.pointStrokeColor
                            }));
                        }
                    }, this);

                    this.buildScale(data.labels);

                    this.eachPoints(function(point, index) {
                        helpers.extend(point, {
                            x: this.scale.calculateX(index),
                            y: this.scale.endPoint
                        });
                        point.save();
                    }, this);

                }, this);

                this.render();
            },

            draw: function(ease) {
                var helpers = Chart.helpers;
                var easingDecimal = ease || 1;
                this.clear();

                var ctx = this.chart.ctx;

                this.scale.draw(easingDecimal);

                helpers.each(this.datasets, function(dataset) {

                    helpers.each(dataset.points, function(point, index) {
                        point.transition({
                            y: this.scale.calculateY(point.value),
                            x: this.scale.calculateX(index)
                        }, easingDecimal);

                    }, this);

                    if (this.options.bezierCurve) {
                        helpers.each(dataset.points, function(point, index) {
                            if (index === 0) {
                                point.controlPoints = helpers.splineCurve(point, point, dataset.points[index + 1], 0);
                            } else if (index >= dataset.points.length - 1) {
                                point.controlPoints = helpers.splineCurve(dataset.points[index - 1], point, point, 0);
                            } else {
                                point.controlPoints = helpers.splineCurve(dataset.points[index - 1], point, dataset.points[index + 1], this.options.bezierCurveTension);
                            }
                        }, this);
                    }

                    if (dataset.dashed) {
                        ctx.setLineDash([10, 5]);
                    } else {
                        ctx.setLineDash([0]);
                    }

                    ctx.lineWidth = this.options.datasetStrokeWidth;
                    ctx.shadowBlur = this.options.shadowBlur;
                    ctx.shadowColor = dataset.strokeShadowColor;
                    ctx.strokeStyle = dataset.strokeColor;

                    var penDown = false;
                    var start = null

                    helpers.each(dataset.points, function(point, index) {
                        if (!point.ignore && !penDown) {
                            ctx.beginPath();
                            penDown = true;
                            start = point;
                        }
                        if (index > 0 && !dataset.points[index - 1].ignore && !point.ignore) {
                            if (this.options.bezierCurve) {
                                ctx.bezierCurveTo(
                                    dataset.points[index - 1].controlPoints.outer.x,
                                    dataset.points[index - 1].controlPoints.outer.y,
                                    point.controlPoints.inner.x,
                                    point.controlPoints.inner.y,
                                    point.x,
                                    point.y
                                );
                            } else {
                                ctx.lineTo(point.x, point.y);
                            }

                        } else if (index === 0 || dataset.points[index - 1].ignore) {
                            ctx.moveTo(point.x, point.y);
                        }

                        if (((dataset.points.length > index + 1 && dataset.points[index + 1].ignore) ||
                            dataset.points.length == index + 1) && !point.ignore) {
                            ctx.stroke();

                            if (this.options.datasetFill) {
                                ctx.lineTo(point.x, this.scale.endPoint);
                                ctx.lineTo(start.x, this.scale.endPoint);
                                ctx.fillStyle = dataset.fillColor;
                                ctx.closePath();
                                if (point.x != start.x) {
                                    ctx.fill();
                                }
                            }
                            penDown = false;
                        }

                    }, this);

                    helpers.each(dataset.points, function(point) {             
                        if (!point.ignore)
                            point.draw();
                    });

                    ctx.shadowBlur = 0;

                }, this);
            }
        });
    }
}