//'use strict';

widget = {
    onData : function(widget, data) {
        data.Events.forEach(event => {
            if ($(`*[data-id="${event.eventId}"]`, widget).length !== 0)
                return;

            $('.container ul', widget).append(
                $('<li>').attr('data-id', event.eventId).attr('data-timestamp', event.timestamp).append(
                    $('<h3>').append(event.message)
                ).append(
                    $('<p>').append(event.detail)
                )
            );
        });

        if (!widget.isInitialised) {
            $.fn.shiftItems = function() {
                let shiftListItems = function($list) {
                    var $first = $list.children('li:first');
                    const eventTime = new moment.utc(parseInt($first.attr('data-timestamp')));
                    const period = new moment().subtract(data.Period, 'seconds');
                    const replay = eventTime.isSameOrAfter(period);

                    if (($list.children().length <= 2) && replay) {
                        setTimeout(() => shiftListItems($list), 5000);
                    } else {
                        $first.animate(
                            { 'margin-left': $list.children().length >= 2 ? -$first.width() : -$list.width() },
                            function() {
                                if (replay) {
                                    $(this).css('margin-left', 0).appendTo($list);
                                } else {
                                    $(this).remove();
                                }
                                setTimeout(() => shiftListItems($list), 5000);
                            }
                        );
                    }
                };

                return this.each(function () {
                    var $that = $(this);
                    setTimeout(() => shiftListItems($that), 5000);
                });
            };

            $('.container ul', widget).shiftItems();
            widget.isInitialised = true;
        }
    }
};